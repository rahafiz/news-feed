//
//  ParseOperation3.h
//  Mole
//
//  Created by Wan Rahafiz on 9/26/12.
//
//

#import <Foundation/Foundation.h>

@class MoleProperties3;
@class URLConnThirdView;

@protocol ParseOperation3Delegate;

@interface ParseOperation3 : NSOperation <NSXMLParserDelegate>
{
@private
    id <ParseOperation3Delegate> delegate;
    
    NSData              *dataToParse;
    
    NSArray             *elementsToParse;
    NSMutableArray      *workingArray;
    NSMutableString     *results;
    
    
    BOOL                storingCharacterData;
    MoleProperties3      *workingEntry;
    //URLConnThirdView   *appDelegate;
}


- (id)initWithData:(NSData *)data delegate:(id <ParseOperation3Delegate>)theDelegate;

@end

@protocol ParseOperation3Delegate
- (void)didFinishParsing:(NSArray *)appList;
- (void)parseErrorOccurred:(NSError *)error;

@end
