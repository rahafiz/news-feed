//
//  SecondParseOperation1.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <Foundation/Foundation.h>

@class MolePropertiesSecond1;
@class SecondURLConn1;

@protocol SecondParseOperation1Delegate;

@interface SecondParseOperation1 : NSOperation <NSXMLParserDelegate>
{
@private
    id <SecondParseOperation1Delegate> delegate;
    
    NSData                *dataToParse;
    
    NSArray               *elementsToParse;
    NSMutableArray        *workingArray;
    NSMutableString       *results;
    
    
    BOOL                  storingCharacterData;
    MolePropertiesSecond1 *workingEntry;
    //SecondURLConn1        *appDelegate;
}


- (id)initWithData:(NSData *)data delegate:(id <SecondParseOperation1Delegate>)theDelegate;

@end

@protocol SecondParseOperation1Delegate
- (void)didFinishParsing:(NSArray *)appList;
- (void)parseErrorOccurred:(NSError *)error;

@end
