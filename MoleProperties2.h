//
//  MoleProperties2.h
//  Mole
//
//  Created by Wan Rahafiz on 9/25/12.
//
//

#import <Foundation/Foundation.h>

@interface MoleProperties2 : NSObject
{
    NSString *title;
    NSString *link;
    NSString *description;
    NSString *language;
    NSString *item;
    NSString *date;
    
    NSString *image2;
    
    NSString *content;
    
    UIImage  *moleIcon;
    
}

#pragma mark - property declaration

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *link;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *language;
@property (nonatomic, retain) NSString *item;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *date;



@property (nonatomic, strong) NSString  *image2;

@property (nonatomic, strong) UIImage   *moleIcon;

@end