//
//  ParseOperation5.m
//  Mole
//
//  Created by Wan Rahafiz on 9/26/12.
//
//

#import "ParseOperation5.h"
#import "MoleProperties5.h"

//set the string for our data in xml file
static NSString *kChannelStr        = @"channel";
static NSString *kTitleStr          = @"title";
static NSString *kLinkStr           = @"link";
static NSString *kDescriptionStr    = @"description";
static NSString *kLanguageStr       = @"language";
static NSString *kItemStr           = @"item";
static NSString *kPubDateStr        = @"pubDate";

@interface ParseOperation5()
{
    
}

@property (nonatomic, readwrite) id <ParseOperation5Delegate> delegate;

@property (nonatomic, retain) NSData            *dataToParse;
@property (nonatomic, retain) NSMutableArray    *workingArray;
@property (nonatomic, retain) NSArray           *elementsToParse;
@property (nonatomic, retain) NSMutableString   *workingPropertyString;


@property (nonatomic, assign) BOOL              storingCharacterData;
@property (nonatomic, retain) MoleProperties5   *workingEntry;

@end

@implementation ParseOperation5


- (id)initWithData:(NSData *)data delegate:(id <ParseOperation5Delegate>)theDelegate
{
    //init our data when we start the parsing
    self = [super init];
    if (self != nil)
    {
        self.dataToParse = data;
        self.delegate = theDelegate;
        self.elementsToParse = [NSArray arrayWithObjects:kChannelStr, kTitleStr,
                                kLinkStr, kDescriptionStr,kLanguageStr,kItemStr,kPubDateStr,nil];
        
        
    }
    return self;
}

- (void)main
{
    
	//array and string for hold data when parsing started
	self.workingArray = [[NSMutableArray alloc]init];
    self.workingPropertyString = [[NSMutableString alloc]init];;
    
    // It's also possible to have NSXMLParser download the data, by passing it a URL, but this is not
	// desirable because it gives less control over the network, particularly in responding to
	// connection errors.
    //
    
    //start parsing
    NSXMLParser *parser = [[NSXMLParser alloc]initWithData:dataToParse];
    [parser setDelegate:self];
    
    //check if the parsing successfully or not
    BOOL success = [parser parse];
    
    if (success) {
        //NSLog(@"NO ERROR5");
    }
    else {
        //NSLog(@"ERROR OCCUR");
    }
    
    
	if (![self isCancelled])
    {
        // notify our AppDelegate that the parsing is complete
        [self.delegate didFinishParsing:self.workingArray];
        
    }
    
    self.workingArray = nil;
    self.workingPropertyString = nil;
    self.dataToParse = nil;
    
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    //NSLog(@"DID START DOCUMENT!");
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    
    
    
    if ([elementName isEqualToString:@"channel"]) {
        
        if (!self.workingArray) {
            self.workingArray = [[NSMutableArray alloc]init];
            
        }
        return;
        
    }
    if ([elementName isEqualToString:kItemStr]){
        
        workingEntry = [[MoleProperties5 alloc]init];
        return;
        
    }
    if ([elementName isEqualToString:kTitleStr]){
        
        results = [[NSMutableString alloc]init];
        
        
    }
    if ([elementName isEqualToString:kLinkStr]) {
        
        results = [[NSMutableString alloc]init];
    }
    
    if ([elementName isEqualToString:kDescriptionStr]) {
        results = [[NSMutableString alloc]init];
    }
    
    if ([elementName isEqualToString:kPubDateStr]) {
        results = [[NSMutableString alloc]init];
    }
    
    if ([elementName isEqualToString:kLanguageStr]) {
        results = [[NSMutableString alloc] init];
    }

    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
{
    
    
    
    if ([elementName isEqualToString:@"channel"]) {
        return;
    }
    if ([elementName isEqualToString:kItemStr])
    {
       // NSLog(@"END ELEMENT: %@", kItemStr);
        [workingArray addObject:workingEntry];
        
        return;
        
        
    }
    if ([elementName isEqualToString:kTitleStr])
    {
        self.workingEntry.title = results;
        results = nil;
        
        
        //NSLog(@"news stand id : %@",workingEntry.title);
    }
    else if ([elementName isEqualToString:kLinkStr])
    {
        
        self.workingEntry.link = results;
        results = nil;
        
        
    }
    else if ([elementName isEqualToString:kDescriptionStr])
    {
        
        workingEntry.description = results;
        
        NSString       *src         = nil;
        NSString       *newsRssFeed = results;
        
        NSScanner      *theScanner  = [NSScanner scannerWithString:newsRssFeed];
        
        [theScanner scanUpToString:@"src" intoString:nil];
        NSCharacterSet *charSet     = [NSCharacterSet characterSetWithCharactersInString:@"\"'"];
        
        [theScanner scanUpToCharactersFromSet:charSet intoString:nil];
        [theScanner scanCharactersFromSet:charSet intoString:nil];
        [theScanner scanUpToCharactersFromSet:charSet intoString:&src];
        
         NSString       *srcStr     = [src stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        workingEntry.image5 = srcStr;
        
        //NSLog(@"Sorce for image is: %@", src);
        
        
        results = nil;
        src = nil;
        srcStr = nil;
        
        
    }
    
    else if ([elementName isEqualToString:kPubDateStr])
    {
        
        
        self.workingEntry.date = results;
        results = nil;
        
        
    }
    
    else if ([elementName isEqualToString:kLanguageStr])
    {
        
        self.workingEntry.language = results;
        results = nil;
        
    }
    
    
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    
    
    if (!results) {
        results = [[NSMutableString alloc]initWithString:string];
    }
    else {
        [results appendString:string];
    }
    
    
    //NSLog(@"Results is: %@", results);
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    [delegate parseErrorOccurred:parseError];
}



@synthesize  dataToParse, workingArray, workingEntry, workingPropertyString, elementsToParse, storingCharacterData,delegate;

@end


