//
//  MolePropertiesSecond1.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <Foundation/Foundation.h>

@interface MolePropertiesSecond1 : NSObject
{
    ////////////////////////////////////Second Parse
    
    NSString *title1;
    NSString *created;
    NSString *body;
    NSMutableArray *linkForReadMore;
    
}

///////////////////////////////////////Second Parse

@property (nonatomic, strong) NSString  *title1;
@property (nonatomic, strong) NSString  *created;
@property (nonatomic, strong) NSString  *body;
@property (nonatomic, strong) NSMutableArray  *linkForReadMore;

@end
