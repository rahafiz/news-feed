//
//  MoleFifthImageCell.m
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import "MoleFifthImageCell.h"

@implementation MoleFifthImageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@synthesize fifthImage = _fifthImage;

@end
