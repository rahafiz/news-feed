//
//  SecondWebView.h
//  Mole
//
//  Created by Lorenzo Jose on 3/31/13.
//
//

#import <UIKit/UIKit.h>

@interface SecondWebView : UIViewController <UIWebViewDelegate>
{
    
}

@property (nonatomic, strong) IBOutlet UIWebView *secondWebView;
@property (nonatomic, strong)          NSURL           *linkToPage;

@end
