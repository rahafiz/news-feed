//
//  FifthWebView.m
//  Mole
//
//  Created by Lorenzo Jose on 3/31/13.
//
//

#import "FifthWebView.h"

@interface FifthWebView ()

@end

@implementation FifthWebView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.title = @"Read More Here";
    
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:_linkToPage];
    
    
    [self.fifthWebView loadRequest:request];
}

-(void)viewDidUnload
{
    [super viewDidUnload];
    self.fifthWebView = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
