//
//  MoleFifthViewController.m
//  Mole
//
//  Created by Wan Rahafiz on 9/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MoleFifthViewController.h"
#import "URLConnFifthView.h"
#import "Constant.h"
#import "MoleProperties5.h"
#import "MolePropertiesSecond5.h"
#import "SecondURLConn5.h"
#import "MoleFifthCustomCell.h"
#import "MoleDetail5VC.h"
#import "PullToRefreshView.h"
#import "MoleFifthAdsCell.h"
#import "MoleFifthLinkCell.h"
#import <unistd.h>


static NSString *const URLName = @"http://mole.my/taxonomy/term/2/4/feed";
static NSString *const SecondURLName = @"http://mole.my/lifestyle_rss.xml";

@interface MoleFifthViewController ()

@property (nonatomic, strong) PullToRefreshView *pull;

@end

@implementation MoleFifthViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Lifestyle", @"Lifestyle");
        self.navigationItem.title = @"Lifestyle";
        self.tabBarItem.image = [UIImage imageNamed:@"lifestyle"];
        [self.tabBarItem setFinishedSelectedImage:self.tabBarItem.image withFinishedUnselectedImage:self.tabBarItem.image];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    URLConnFifthView *connController  = [[URLConnFifthView alloc]init];
    [connController startConn:[NSURL URLWithString:URLName]];
    
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(refreshData5:)
                                                name:kReceiveData5
                                              object:nil];
    
    
    self.navigationItem.title = nil;
    
    //Create loading object once
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        
        // Set the hud to display with a color
        HUD.color          = [UIColor orangeColor];
        HUD.dimBackground  = YES;
        
        HUD.delegate  = self;
        HUD.labelText = @"Loading";
 
        [HUD show:YES];
    });
   
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Create a view of the standard size at the top of the screen.
    // Available AdSize constants are explained in GADAdSize.h.
    bannerView_ = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner];
    
    //NSLog(@"BANNER IS%@", bannerView_);
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = kAdMobPubID;
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
  
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
  
}


#pragma mark - PullToRefresh delegate method

-(void)pullToRefreshViewShouldRefresh:(PullToRefreshView *)view
{
    [self performSelectorInBackground:@selector(reloadTableData) withObject:nil];
}

-(void)reloadTableData
{
    [pull setState:PullToRefreshViewStateLoading];
    [self performSelectorOnMainThread:@selector(viewDidLoad) withObject:nil waitUntilDone:NO];
    
    
}

//-(void)foregroundRefresh:(NSNotification *)notification
//{
//  
//        self.dataTbl5.contentOffset = CGPointMake(0, -65);
//        [pull setState:PullToRefreshViewStateLoading];
//        [self performSelectorInBackground:@selector(reloadTableData) withObject:nil];
// 
//   
//}

#pragma mark -
#pragma mark - NSNnotification method

-(void)refreshData5:(NSNotification *)object
{
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:kReceiveData5
                                                 object:nil];
    
    self.pageArrFifthView = (NSArray *)[object object];
    
    SecondURLConn5 *urlContrl = [[SecondURLConn5 alloc]init];
    [urlContrl startConnection:[NSURL URLWithString:SecondURLName]];
    
    //NSLog(@"PAGE ARRAY IS: %d", [pageArrFifthView count]);

    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(receiveDataSecond:)
                                                name:kReceiveDataSecond5
                                              object:nil];
}

-(void)receiveDataSecond:(NSNotification*)body
{
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:kReceiveDataSecond5
                                                 object:nil];
    
    self.pageArrSecondParse = (NSArray*)[body object];
    
    //NSLog(@"Data For Second Parse is:%@", self.pageArrSecondParse);
    
    [self performSelector:@selector(loadImagesForOnscreenRows) withObject:nil afterDelay:1.0];
    
    static dispatch_once_t oncePull;
    dispatch_once(&oncePull, ^{
        
        pull = [[PullToRefreshView alloc]initWithScrollView:(UIScrollView*)self.dataTbl5];
        [pull setDelegate:self];
        [self.dataTbl5 addSubview:pull];
        
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(foregroundRefresh:)
//                                                     name:UIApplicationWillEnterForegroundNotification
//                                                   object:nil];
        
    });
    
    [pull finishedLoading];
    [pull setState:PullToRefreshViewStateNormal];
    [self.dataTbl5 reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[HUD removeFromSuperview];
	HUD = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

#pragma mark - tableview datasouce

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([self.pageArrFifthView count] > 30)
    {
        return 30;
    }
    else
        return [self.pageArrFifthView count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 3)
    {
        return 50;
    }
    return 86;
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier  = @"FifthCustom";
    static NSString *PlaceHolderIdentifier = @"placeHolder";
    static NSString *AdsIdentifier = @"FifthAds";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PlaceHolderIdentifier];
    int nodeCount = [self.pageArrFifthView count];
    
    if (nodeCount == 0 && indexPath.row == 0)
    {
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:PlaceHolderIdentifier];
        }
        
        cell.textLabel.text = @"Loading";
        //cell.imageView.image = [UIImage imageNamed:@"appleLogo.png"];
        
        return cell;
    }
    
    else
    {
        if (indexPath.row == 3)
        {
            MoleFifthAdsCell *cell = (MoleFifthAdsCell *)[tableView dequeueReusableCellWithIdentifier:AdsIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MoleFirstAdsCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            [cell addSubview:bannerView_];
            
            // Initiate a generic request to load it with an ad.
            [bannerView_ loadRequest:[GADRequest request]];
            return cell;
        }
        

        
        MoleFifthCustomCell *cell = (MoleFifthCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MoleFifthCustomCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
            
        }
        
        MoleProperties5 *moleCntrl = [self.pageArrFifthView objectAtIndex:indexPath.row];
        
        cell.titleLabel.textColor = [UIColor blackColor];
        cell.dateLabel.textColor  = [UIColor grayColor];
        cell.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17.0];
        cell.titleLabel.text = moleCntrl.title;
        cell.dateLabel.text  = moleCntrl.date;
    
        
        if (moleCntrl.moleIcon)
        {
            cell.thumbnailImageView.image = moleCntrl.moleIcon;
            return cell;
        }
        else
        {
            cell.thumbnailImageView.image = [UIImage imageNamed:@"moleTableIcon.png"];
        }
        
        return cell;
        
    }
    
    return cell;

}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //NSLog(@"Did Ened Decelerate");
    
    [self loadImagesForOnscreenRows];
}


#pragma mark - tableview delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    viewController_ = [[MoleDetail5VC  alloc]initWithNibName:@"MoleDetail5VC" bundle:nil];
    
    MoleProperties5 *controllerMole = [self.pageArrFifthView objectAtIndex:indexPath.row];
    
    MolePropertiesSecond5 *moleCntroller = [self.pageArrSecondParse objectAtIndex:indexPath.row];
    
     _LinkForRead = [[NSMutableArray alloc]init];
 
    NSData *ipData = [[NSData alloc]initWithContentsOfURL:
                      [NSURL URLWithString:controllerMole.image5]];
    
    UIImage *image = [UIImage imageWithData:ipData];

    [viewController_ setStoryTitle:moleCntroller.title5];
    [viewController_ setDescription:moleCntroller.body];
    [viewController_ setPageImage5:image];
    [viewController_ setLink:controllerMole.link];
    
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:moleCntroller.body options:0 range:NSMakeRange(0, [moleCntroller.body length])];
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            NSURL *url = [match URL];
            
            if (_LinkForRead != nil)
            {
                [viewController_ setLinkURL:url];
                [_LinkForRead removeAllObjects];
                [_LinkForRead addObject:url];
                [viewController_ setLinkForReadMore:_LinkForRead];
            } else {
                
                [viewController_ setLinkURL:url];
                [_LinkForRead addObject:url];
                [viewController_ setLinkForReadMore:_LinkForRead];
                //NSLog(@"found URL: %@", url);
                
            }
            
        }
    }
    
    
    
    //NSLog(@"LINK FOR READ %@", _LinkForRead);

    viewController_.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController_ animated:YES];
    
    [dataTbl5 deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Load image method

- (void)loadImagesForOnscreenRows
{
    
    if ([self.pageArrFifthView count] > 0)
    {
        NSArray *visiblePaths         = [self.dataTbl5 indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            
            MoleProperties5 *moleItem = [self.pageArrFifthView objectAtIndex:indexPath.row];
            
            if (!queue)
            {
                queue = dispatch_queue_create("image_queue", NULL);
            }
            
            dispatch_async(queue, ^
                           {
                               
                               NSString *imageString = moleItem.image5;
                               
                               //NSLog(@"AAAAAAA: %@",moleItem.image3);
                               
                               NSData   *data        = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageString]];
                               
                               UIImage  *anImage     = [UIImage imageWithData:data];
                               
                               MoleFifthCustomCell *cell = (MoleFifthCustomCell*)[self.dataTbl5 cellForRowAtIndexPath:indexPath];
                               
                               //dispatch_async on the main queue to update the UI
                               dispatch_async(dispatch_get_main_queue(), ^
                                              {
                                                  
                                                  if (anImage != nil)
                                                  {
                                                      [cache setObject:anImage forKey:moleItem.moleIcon];
                                                  
                                                  
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                                  
                                                      if (indexPath.row != 3)
                                                      {
                                                          cell.thumbnailImageView.image = anImage;
                                                      }
                                                      
                                                  }
                                                  
                                                 // NSLog(@"Index Path Reload : %@" ,anImage);
                                                  //[self.dataTbl2 reloadRowsAtIndexPaths:[NSArray arrayWithArray:visiblePaths] withRowAnimation:UITableViewRowAnimationNone];
                                                  //[table2 reloadRowsAtIndexPaths:[NSArray arrayWithArray:visiblePaths] withRowAnimation:UITableViewRowAnimationNone];
                                              });
                           });
        }
        
    }
}



@synthesize dataTbl5;
@synthesize pageArrFifthView;
@synthesize viewController = viewController_;
@synthesize pageArrSecondParse;
@synthesize cache;
@synthesize pull;

@end
