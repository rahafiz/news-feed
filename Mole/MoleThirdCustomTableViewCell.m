//
//  MoleThirdCustomTableViewCell.m
//  Mole
//
//  Created by Wan Rahafiz on 9/26/12.
//
//

#import "MoleThirdCustomTableViewCell.h"

@implementation MoleThirdCustomTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@synthesize titleLabel         = titleLabel_;
@synthesize thumbnailImageView = thumbnailImageView_;
@synthesize dateLabel          = dateLabel_;


@end
