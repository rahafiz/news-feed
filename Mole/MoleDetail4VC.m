//
//  MoleDetail4VC.m
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import "MoleDetail4VC.h"
#import "Constant.h"
#import "MoleFourthCustomCell.h"
#import "MoleFourthTitleCell.h"
#import "MoleFourthDetailCell.h"
#import "MolePropertiesSecond4.h"
#import "MoleFourthImageCell.h"
#import "AddThis.h"
#import "MoleFourthLinkCell.h"

#define FONTSIZE 15.0f
#define WORD_WRAP_MODE NSLineBreakByWordWrapping
#define LABEL_WIDTH 280

#define SYSTEM_VERSION_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface MoleDetail4VC ()

@end

@implementation MoleDetail4VC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.detailTbl4.backgroundColor = [UIColor clearColor];
    
    //Facebook connect settings
	//CHANGE THIS FACEBOOK API KEY TO YOUR OWN!!
	[AddThisSDK setFacebookAPIKey:kFBAPIKEY];
	[AddThisSDK setFacebookAuthenticationMode:ATFacebookAuthenticationTypeFBConnect];
    
    //AddThis publisher ID
    [AddThisSDK setAddThisPubId:kAdThisPubID];
    [AddThisSDK setAddThisApplicationId:kAdThisAppID];
    
    //CHANGE THIS TWITTER API KEYS TO YOUR OWN!!
	[AddThisSDK setTwitterConsumerKey:kTwitterConKey];
    [AddThisSDK setTwitterConsumerSecret:kTwitterConSecret];
	//[AddThisSDK setTwitterCallBackURL:@"http://addthis.com/mobilesdk/twittertesting"];
    [AddThisSDK setTwitterViaText:@"The_Mole"];
	
    //[AddThisSDK setTwitPicAPIKey:@"45149651ec391a4e2b8135b43a63346b"];
    [AddThisSDK setTwitterAuthenticationMode:ATTwitterAuthenticationTypeOAuth];
    
    [AddThisSDK canUserEditServiceMenu:YES];
	[AddThisSDK canUserReOrderServiceMenu:YES];
	[AddThisSDK setDelegate:self];
    
    //NSLog(@"NENEN IS: %@", _linkForReadMore);

    
    
}

-(void)viewDidUnload
{
    [super viewDidUnload];
    
    [_linkForReadMore removeAllObjects];
    _LinkURL = nil;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Create a view of the standard size at the top of the screen.
    // Available AdSize constants are explained in GADAdSize.h.
    bannerView_ = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner];
    
    //NSLog(@"BANNER IS%@", bannerView_);
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = kAdMobPubID;
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
    [self.view addSubview:bannerView_];
    
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:[GADRequest request]];
}

//-(void)detailData:(NSNotification*)object
//{
//    [[NSNotificationCenter defaultCenter]removeObserver:self
//                                                   name:kDetailData4
//                                                 object:nil];
//    self.mArrayDetails = (NSMutableArray*)[object object];
//    
//    
//    NSLog(@"MARRAYDETAILS IS:%@", self.mArrayDetails);
//    
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark table view data source

-(NSInteger)numberOfSectionInTableView:(UITableView*)tableView
{
    return [mArraySection count];
    
    //NSLog(@"MARRAYSECTION IS: %d", [mArraySection count]);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 68;
    }
    else if (indexPath.row == 1)
    {
        return 150;
    }
    else if (indexPath.row == 2)
    {
        NSString *text = description;
        
        CGFloat height = [self getLabelHeightWithDesc:text
                                                 font:[UIFont systemFontOfSize:FONTSIZE]
                                        lineBreakMode:WORD_WRAP_MODE
                                           labelWidth:LABEL_WIDTH];
        
        return height + 20;
    }
    else if (indexPath.row == 3)
    {
        return 53;
    }
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //	NSLog(@"Number of rows in event");
	
    if (_linkForReadMore.count == 1)
    {
        
        return 4;
        
    } else {
        
        return 3;
    }
    
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    
    if(indexPath.row == 0){
        static NSString *TitleIdentifierCell = @"FourthTitle";
        MoleFourthTitleCell *cell =(MoleFourthTitleCell*) [detailTbl4 dequeueReusableCellWithIdentifier:TitleIdentifierCell];
        if(cell==nil){
            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"MoleFourthTitleCell" owner:self options:nil];
            cell = (MoleFourthTitleCell*)[nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.backgroundColor = [UIColor clearColor];
        cell.opaque = NO;
        
        //Custom cell with whatever
        [cell.titleLabel setText:storyTitle];
        
        return cell;
    }
    
    else if (indexPath.row == 1)
    {
        static NSString *ImageIdentifier = @"FourthImage";
        MoleFourthImageCell *cell = (MoleFourthImageCell*)[detailTbl4 dequeueReusableCellWithIdentifier:ImageIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"MoleFourthImageCell" owner:self options:nil];
            cell = (MoleFourthImageCell*)[nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        cell.backgroundColor = [UIColor clearColor];
        cell.opaque = NO;
        cell.backgroundView = nil;
        
        if (pageImage4 != nil)
        {
             cell.fourthImage.image = pageImage4;
             return cell;
        }
        else
        {
            cell.fourthImage.image = [UIImage imageNamed:@"detailPlaceHolder.png"];
        }
       
        
        return cell;
        
    }
    //We use CellType2 xib for other rows
    else if (indexPath.row == 2)
    {
        static NSString *DetailIdentifierCell = @"FourthDetail";
        //UILabel *label = nil;
        MoleFourthDetailCell *cell =(MoleFourthDetailCell*) [detailTbl4 dequeueReusableCellWithIdentifier:DetailIdentifierCell];
        if(cell==nil){
            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"MoleFourthDetailCell" owner:self options:nil];
            cell = (MoleFourthDetailCell*)[nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            _labelForStr = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, LABEL_WIDTH, 44)];
            //[label setLineBreakMode:UILineBreakModeWordWrap];
            //[label setMinimumFontSize:FONTSIZE];
            [_labelForStr setNumberOfLines:0];
            [_labelForStr setFont:[UIFont systemFontOfSize:FONTSIZE]];
            [_labelForStr setTag:1];
            
            [[cell contentView] addSubview:_labelForStr];
            
        }
        cell.backgroundColor = [UIColor clearColor];
        cell.opaque = NO;
        cell.backgroundView = nil;

        //Custom cell with whatever
        _labelForStr.font = [UIFont fontWithName:@"Helvetica" size:15.0];
       
        
        CGFloat height = [self getLabelHeightWithDesc:description
                                                 font:[UIFont systemFontOfSize:FONTSIZE]
                                        lineBreakMode:WORD_WRAP_MODE
                                           labelWidth:LABEL_WIDTH];
        
        [_labelForStr setFrame:CGRectMake(10, 10, LABEL_WIDTH , height)];
        
        if (_LinkURL == nil)
        {
            [_labelForStr setText:description];
            
            
            
        } else {
            
            NSString *finalStr2    = [description stringByReplacingOccurrencesOfString:@"Read HERE for the full article:" withString:@""];
            
            NSString *finalStr  = [finalStr2 stringByReplacingOccurrencesOfString:[_LinkURL absoluteString] withString:@""];
            
            [_labelForStr setText:finalStr];
            
            
            
        }
        
        
        
        return cell;
    }
    
    else if (_linkForReadMore.count == 1 && indexPath.row == 3)
    {
        static NSString *LinkIdentifierCell = @"FourthLink";
        MoleFourthLinkCell *cell = (MoleFourthLinkCell*)[detailTbl4 dequeueReusableCellWithIdentifier:LinkIdentifierCell];
        
        if (cell == nil)
        {
            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"MoleFourthLinkCell" owner:self options:nil];
            cell = (MoleFourthLinkCell*)[nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        return cell;
        
        
    }
    
    return cell;
    
    
	
}

-(CGFloat)getLabelHeightWithDesc:(NSString*)desc
                            font:(UIFont*)font
                   lineBreakMode:(NSLineBreakMode)lineBreakMode
                      labelWidth:(CGFloat)width
{
    CGSize contentSize = CGSizeMake(width, 999999999.0f);
    
    CGSize size = [desc sizeWithFont:font
                   constrainedToSize:contentSize
                       lineBreakMode:lineBreakMode];
    
    CGFloat height = MAX(size.height, 44.0);
    
    return height;
}

#pragma mark -
#pragma mark Table view delegate method

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 3)
    {
        _fourthWebVC = [[FourthWebView alloc]initWithNibName:@"FourthWebView"
                                                    bundle:nil];
        [_fourthWebVC setLinkToPage:_LinkURL];
        
        [self.navigationController pushViewController:_fourthWebVC animated:YES];
    }
    
    
}

#pragma mark -
#pragma mark - UIActionSheet delegate method

-(IBAction)showActionSheet:(id)sender
{
    UIActionSheet *popupQuery = [[UIActionSheet alloc]initWithTitle:@""
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:@"Share Via Facebook",@"Share Via Twitter",@"Share Via Email", nil];
    
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [popupQuery showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
		case 0:
			//NSLog(@"Facebook");
            
            [self performSelectorInBackground:@selector(facebookButtonClicked:) withObject:nil];
			break;
		case 1:
            //NSLog(@"Twitter");
            
            [self performSelectorInBackground:@selector(twitterButtonClicked:) withObject:nil];
			break;
		case 2:
            //NSLog(@"Email");
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
            {
                [self performSelectorInBackground:@selector(openMail:) withObject:nil];
            }
            else if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
            {
                [self performSelectorInBackground:@selector(emailButtonClicked:) withObject:nil];
            }
			break;
		case 3:
            //NSLog(@"Cancel");
			break;
	}
}

#pragma mark -
#pragma mark - Facebook/Twitter/Email method

-(void)facebookButtonClicked:(id)sender
{
	//share to facebook
	[AddThisSDK shareURL:link
			 withService:@"facebook"
				   title:storyTitle
			 description:@""];
    
    
}

-(void)twitterButtonClicked:(id)sender
{
	//share to twitter
	[AddThisSDK shareURL:self.link
			 withService:@"twitter"
				   title:storyTitle
			 description:@""];
}

-(void)emailButtonClicked:(id)sender
{
	//share to native email
	[AddThisSDK shareURL:self.link
			 withService:@"mailto"
				   title:storyTitle
			 description:@"From The Mole"];
}

//ios 6

-(void)openMail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        
        if ([MFMailComposeViewController canSendMail])
        {
            //NSLog(@"Massage will be send");
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Failure"
                                                           message:@"Your Device"
                                                          delegate:nil cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:storyTitle];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"", @"", nil];
        [mailer setToRecipients:toRecipients];
        
        UIImage *myImage = [UIImage imageNamed:@"moleTrueLogo.png"];
        NSData *imageData = UIImagePNGRepresentation(myImage);
        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"The Mole"];
        
        NSString *seprator = @"\n------\n";
        
        NSString *emailBody = [NSString stringWithFormat:@"%@\n%@\n%@\n", link,seprator, @"From The Mole"];
        [mailer setMessageBody:emailBody isHTML:YES];
        
        // only for iPad
        // mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentViewController:mailer animated:YES completion:nil];
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        
    }
    
}


#pragma mark - MFMailComposeController delegate


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
			//NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued");
			break;
		case MFMailComposeResultSaved:
			//NSLog(@"Mail saved: you saved the email message in the Drafts folder");
			break;
		case MFMailComposeResultSent:
			//NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send the next time the user connects to email");
			break;
		case MFMailComposeResultFailed:
			//NSLog(@"Mail failed: the email message was nog saved or queued, possibly due to an error");
			break;
		default:
			//NSLog(@"Mail not sent");
			break;
	}
    
	//[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}



@synthesize titleLbl,contentText, arrayDetailContent, mArrayDetails, detailTbl4;
@synthesize storyTitle,description, pageImage4, link;

@end
