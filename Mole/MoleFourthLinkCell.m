//
//  MoleFourthLinkCell.m
//  Mole
//
//  Created by Lorenzo Jose on 3/31/13.
//
//

#import "MoleFourthLinkCell.h"

@implementation MoleFourthLinkCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
