//
//  MoleSecondImageCell.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <UIKit/UIKit.h>

@interface MoleSecondImageCell : UITableViewCell
{
    
}

@property (nonatomic, strong) IBOutlet UIImageView *secondImage;

@end
