//
//  MoleThirdViewControllerViewController.m
//  Mole
//
//  Created by Wan Rahafiz on 9/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MoleThirdViewControllerViewController.h"
#import "URLConnThirdView.h"
#import "SecondURLConn3.h"
#import "Constant.h"
#import "MoleProperties3.h"
#import "MolePropertiesSecond3.h"
#import "MoleThirdCustomTableViewCell.h"
#import "MoleSecondViewController.h"
#import "MoleDetail3ViewController.h"
#import "PullToRefreshView.h"
#import "MoleThirdCustomDetailCell.h"
#import "MoleThirdDetailsCustomCell.h"
#import "MoleThirdAdsCell.h"
#import "MoleThirdLinkCell.h"
#import <unistd.h>


@interface MoleThirdViewControllerViewController()

@property (nonatomic, strong) PullToRefreshView *pull;

@end



//static NSString *const URLName = @"http://mole.my/taxonomy/term/1/4/feed";
static NSString *const URLName = @"http://mole.my/taxonomy/term/1/4/feed";
static NSString *const SecondURLName = @"http://mole.my/business_rss.xml";

@implementation MoleThirdViewControllerViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Business", @"Business");
        self.navigationItem.title = @"Business";
        self.tabBarItem.image = [UIImage imageNamed:@"sale"];
        [self.tabBarItem setFinishedSelectedImage:self.tabBarItem.image withFinishedUnselectedImage:self.tabBarItem.image];
         
       
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    URLConnThirdView *connController  = [[URLConnThirdView alloc]init];
    [connController startConn:[NSURL URLWithString:URLName]];
    
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(refreshData3:)
                                                name:kReceiveData3
                                              object:nil];

    
    
    self.navigationItem.title = nil;
    
    //Create loading object once
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        
        // Set the hud to display with a color
        HUD.color          = [UIColor orangeColor];
        HUD.dimBackground  = YES;
        
        HUD.delegate  = self;
        HUD.labelText = @"Loading";
        
        [HUD show:YES];
    });
   
  
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Create a view of the standard size at the top of the screen.
    // Available AdSize constants are explained in GADAdSize.h.
    bannerView_ = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner];
    
    //NSLog(@"BANNER IS%@", bannerView_);
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = kAdMobPubID;
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}


#pragma mark - PullToRefresh delegate method

-(void)pullToRefreshViewShouldRefresh:(PullToRefreshView *)view
{
    [self performSelectorInBackground:@selector(reloadTableData) withObject:nil];
}

-(void)reloadTableData
{
    [pull setState:PullToRefreshViewStateLoading];
    [self performSelectorOnMainThread:@selector(viewDidLoad) withObject:nil waitUntilDone:NO];
        
    
}

//-(void)foregroundRefresh:(NSNotification *)notification
//{
//  
//        self.dataTbl3.contentOffset = CGPointMake(0, -65);
//        [pull setState:PullToRefreshViewStateLoading];
//        [self performSelectorInBackground:@selector(reloadTableData) withObject:nil];
//  
//  
//}

#pragma mark -
#pragma mark - NSNnotification method

-(void)refreshData3:(NSNotification *)object
{
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:kReceiveData3
                                                 object:nil];
    
    self.pageArrThirdView = (NSArray *)[object object];
    
    SecondURLConn3 *urlContrl = [[SecondURLConn3 alloc]init];
    [urlContrl startConnection:[NSURL URLWithString:SecondURLName]];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(receiveDataSecond:)
                                                name:kReceiveDataSecond3
                                              object:nil];
    
   
    
}

-(void)receiveDataSecond:(NSNotification*)body
{
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:kReceiveDataSecond3
                                                 object:nil];
    
    self.pageArrSecondParse = (NSArray*)[body object];
    
    //NSLog(@"Data For Second Parse is:%@", self.pageArrSecondParse);
    
    [self performSelector:@selector(loadImagesForOnscreenRows) withObject:nil afterDelay:1.0];
    
    static dispatch_once_t oncePull;
    dispatch_once(&oncePull, ^{
        
        pull = [[PullToRefreshView alloc]initWithScrollView:(UIScrollView*)self.dataTbl3];
        [pull setDelegate:self];
        [self.dataTbl3 addSubview:pull];
        
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(foregroundRefresh:)
//                                                     name:UIApplicationWillEnterForegroundNotification
//                                                   object:nil];
        
    });
    
    [pull finishedLoading];
    [pull setState:PullToRefreshViewStateNormal];
    [self.dataTbl3 reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
 
   
}

#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[HUD removeFromSuperview];
	HUD = nil;
   
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - tableview datasouce

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([self.pageArrThirdView count] > 30)
    {
        return 30;
    }
    else
       return [self.pageArrThirdView count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 3)
    {
        return 50;
    }
    return 86;
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier  = @"ThirdCustomTableCell";
    static NSString *PlaceHolderIdentifier = @"placeHolder";
    static NSString *AdsIdentifier = @"ThirdAds";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PlaceHolderIdentifier];    
    int nodeCount = [self.pageArrThirdView count];
    
    if (nodeCount == 0 && indexPath.row == 0)
    {
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:PlaceHolderIdentifier];
        }
        
        cell.textLabel.text = @"Loading";
        //cell.imageView.image = [UIImage imageNamed:@"appleLogo.png"];
        
        return cell;
    }
    
    else
    {
        if (indexPath.row == 3)
        {
            MoleThirdAdsCell *cell = (MoleThirdAdsCell *)[tableView dequeueReusableCellWithIdentifier:AdsIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MoleFirstAdsCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            [cell addSubview:bannerView_];
            
            // Initiate a generic request to load it with an ad.
            [bannerView_ loadRequest:[GADRequest request]];
            return cell;
        }
        

        
        MoleThirdCustomTableViewCell *cell = (MoleThirdCustomTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
       if (cell == nil)
       {
           NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MoleThirdCustomTableCell" owner:self options:nil];
           cell = [nib objectAtIndex:0];
    
        
       }
    
           MoleProperties3 *moleCntrl = [self.pageArrThirdView objectAtIndex:indexPath.row];
    
            cell.titleLabel.textColor = [UIColor blackColor];
            cell.dateLabel.textColor  = [UIColor grayColor];
            cell.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17.0];
            cell.titleLabel.text = moleCntrl.title;
            cell.dateLabel.text  = moleCntrl.date;

            if (moleCntrl.moleIcon)
            {
                cell.thumbnailImageView.image = moleCntrl.moleIcon;
                return cell;
            }
            else
            {
                cell.thumbnailImageView.image = [UIImage imageNamed:@"moleTableIcon.png"];
            }
        
            return cell;
    
    }
    
    return cell;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //NSLog(@"Did Ened Decelerate");
    
    [self loadImagesForOnscreenRows];
}


#pragma mark - tableview delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    viewController_ = [[MoleDetail3ViewController  alloc]initWithNibName:@"MoleDetail3ViewController" bundle:nil];
    
    MoleProperties3 *controllerMole = [self.pageArrThirdView objectAtIndex:indexPath.row];
    
    MolePropertiesSecond3 *moleCntroller = [self.pageArrSecondParse objectAtIndex:indexPath.row];
    
     _LinkForRead = [[NSMutableArray alloc]init];
    
    NSData *ipData = [[NSData alloc]initWithContentsOfURL:
                      [NSURL URLWithString:controllerMole.image3]];
    
    UIImage *image = [UIImage imageWithData:ipData];
    
    //[viewController_.pageImage setImage:image];
    [viewController_.titleLbl setText:moleCntroller.title3];
    [viewController_.contentText setText:moleCntroller.body];
    [viewController_ setStoryTitle:moleCntroller.title3];
    [viewController_ setDescription:moleCntroller.body];
    [viewController_ setPageImage3:image];
    [viewController_ setLink:controllerMole.link];
    
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:moleCntroller.body options:0 range:NSMakeRange(0, [moleCntroller.body length])];
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            NSURL *url = [match URL];
            
            if (_LinkForRead != nil)
            {
                [viewController_ setLinkURL:url];
                [_LinkForRead removeAllObjects];
                [_LinkForRead addObject:url];
                [viewController_ setLinkForReadMore:_LinkForRead];
            } else {
                
                [viewController_ setLinkURL:url];
                [_LinkForRead addObject:url];
                [viewController_ setLinkForReadMore:_LinkForRead];
                //NSLog(@"found URL: %@", url);
                
            }
            
        }
    }
    
    
    
    //NSLog(@"LINK FOR READ %@", _LinkForRead);
    
    viewController_.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController_ animated:YES];
    
    [dataTbl3 deselectRowAtIndexPath:indexPath animated:YES];
   
   
}

#pragma mark - Load image method

- (void)loadImagesForOnscreenRows
{
    
    if ([self.pageArrThirdView count] > 0)
    {
        NSArray *visiblePaths         = [self.dataTbl3 indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            
            MoleProperties3 *moleItem = [self.pageArrThirdView objectAtIndex:indexPath.row];
            
            if (!queue)
            {
                queue = dispatch_queue_create("image_queue", NULL);
            }
            
            dispatch_async(queue, ^
                           {
                    
                               NSString *imageString = moleItem.image3;
                               
                               //NSLog(@"AAAAAAA: %@",moleItem.image3);
                               
                               NSData   *data        = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageString]];
                               
                               UIImage  *anImage     = [UIImage imageWithData:data];
                               
                               MoleThirdCustomTableViewCell *cell = (MoleThirdCustomTableViewCell*)[self.dataTbl3 cellForRowAtIndexPath:indexPath];
                               
                               //dispatch_async on the main queue to update the UI
                               dispatch_async(dispatch_get_main_queue(), ^
                                              {
                                                  
                                                  if (anImage != nil)
                                                  {
                                                      [cache setObject:anImage forKey:moleItem.moleIcon];
                                                  
                                                  
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                                 
                                                      if (indexPath.row != 3)
                                                      {
                                                          cell.thumbnailImageView.image = anImage;
                                                      }

                                                  }
                                                  
                                                  //NSLog(@"Index Path Reload : %@" ,anImage);
                                                  //[self.dataTbl3 reloadRowsAtIndexPaths:[NSArray arrayWithArray:visiblePaths] withRowAnimation:UITableViewRowAnimationNone];
                                                  //[table2 reloadRowsAtIndexPaths:[NSArray arrayWithArray:visiblePaths] withRowAnimation:UITableViewRowAnimationNone];
                                              });
                           });
        }
        
    }
}

//#pragma mark - URL Request method
//
//-(void)requestURL:(NSURL*)url
//{
//    
//    [[NSNotificationCenter defaultCenter]postNotificationName:kLoadRequest
//                                                       object:url
//                                                     userInfo:nil];
//}
//
//#pragma mark - UIWebViewDelegate method
//
//- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
//	//CAPTURE USER LINK-CLICK.
//	if (navigationType == UIWebViewNavigationTypeLinkClicked) {
//		
//		
//		
//	}
//	return YES;
//}

@synthesize dataTbl3;
@synthesize pageArrThirdView;
@synthesize viewController = viewController_;
@synthesize cache;
@synthesize pageArrSecondParse;
@synthesize pull;


@end
