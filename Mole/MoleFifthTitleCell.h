//
//  MoleFifthTitleCell.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <UIKit/UIKit.h>

@interface MoleFifthTitleCell : UITableViewCell
{
    
}

#pragma mark - property declaration

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@end
