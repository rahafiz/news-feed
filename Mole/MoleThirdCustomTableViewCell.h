//
//  MoleThirdCustomTableViewCell.h
//  Mole
//
//  Created by Wan Rahafiz on 9/26/12.
//
//

#import <UIKit/UIKit.h>

@interface MoleThirdCustomTableViewCell : UITableViewCell
{
    
}

#pragma mark - property declaration

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnailImageView;


@end
