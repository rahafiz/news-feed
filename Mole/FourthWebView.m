//
//  FourthWebView.m
//  Mole
//
//  Created by Lorenzo Jose on 3/31/13.
//
//

#import "FourthWebView.h"

@interface FourthWebView ()

@end

@implementation FourthWebView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.title = @"Read More Here";
    
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:_linkToPage];
    
    
    [self.fourthWebView loadRequest:request];
}

-(void)viewDidUnload
{
    [super viewDidUnload];
    self.fourthWebView = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
