//
//  MoleFifthViewController.h
//  Mole
//
//  Created by Wan Rahafiz on 9/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToRefreshView.h"
#import "MBProgressHUD.h"
#import "GADBannerView.h"

@class MoleDetail5VC;

@interface MoleFifthViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,PullToRefreshViewDelegate,MBProgressHUDDelegate>
{
    IBOutlet MoleDetail5VC *viewController;
    
    dispatch_queue_t queue;
    
    MBProgressHUD *HUD;
    
    GADBannerView *bannerView_;
}

#pragma mark - property declaration

@property (nonatomic, strong) IBOutlet UITableView   *dataTbl5;

@property (nonatomic, strong)          NSArray       *pageArrFifthView;

@property (nonatomic, strong)          NSArray       *pageArrSecondParse;

@property (nonatomic, strong)          NSCache       *cache;

@property (nonatomic, strong)          MoleDetail5VC *viewController;

@property (nonatomic, strong)          NSMutableArray *LinkForRead;

#pragma mark - method declaration

//-(void)requestURL:(NSURL*)url;
//
//-(void)reloadTableData;
//
//-(void)foregroundRefresh:(NSNotification*)notification;

-(void)reloadTableData;

//-(void)foregroundRefresh:(NSNotification*)notification;

//- (void)myTask;

@end
