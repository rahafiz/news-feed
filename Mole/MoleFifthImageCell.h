//
//  MoleFifthImageCell.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <UIKit/UIKit.h>

@interface MoleFifthImageCell : UITableViewCell
{
    
}

@property (nonatomic, strong) IBOutlet UIImageView *fifthImage;

@end
