//
//  MoleSecondViewController.m
//  Mole
//
//  Created by Wan Rahafiz on 9/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MoleSecondViewController.h"
#import "URLConnSecondView.h"
#import "Constant.h"
#import "MoleProperties2.h"
#import "MolePropertiesSecond2.h"
#import "SecondURLConn2.h"
#import "MoleSecondCustomCell.h"
#import "MoleSecondAdsCell.h"
#import "MoleSecondLinkCell.h"
#import "MoleDetail2VC.h"
#import "PullToRefreshView.h"
#import <unistd.h>


static NSString *const URLName = @"http://mole.my/taxonomy/term/3/4/feed";
static NSString *const SecondURLName = @"http://mole.my/politics_rss.xml";

@interface MoleSecondViewController ()

@property (nonatomic, strong) PullToRefreshView *pull;

@end

@implementation MoleSecondViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        
        self.title = NSLocalizedString(@"Politics", @"Politics");
        self.navigationItem.title = @"Politics";
        self.tabBarItem.image = [UIImage imageNamed:@"politics"];
        [self.tabBarItem setFinishedSelectedImage:self.tabBarItem.image withFinishedUnselectedImage:self.tabBarItem.image];
        
    }
    return self;
}
							
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    URLConnSecondView *connController  = [[URLConnSecondView alloc]init];
    [connController startConn:[NSURL URLWithString:URLName]];
  
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(refreshData2:)
                                                name:kReceiveData2
                                              object:nil];
    
    self.navigationItem.title = nil;
    
    
    //Create loading object once
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        
        // Set the hud to display with a color
        HUD.color          = [UIColor orangeColor];
        HUD.dimBackground  = YES;
        
        HUD.delegate  = self;
        HUD.labelText = @"Loading";
        
        [HUD show:YES];
    });
  
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Create a view of the standard size at the top of the screen.
    // Available AdSize constants are explained in GADAdSize.h.
    bannerView_ = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner];
    
    //NSLog(@"BANNER IS%@", bannerView_);
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = kAdMobPubID;
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}


#pragma mark - PullToRefresh delegate method

-(void)pullToRefreshViewShouldRefresh:(PullToRefreshView *)view
{
    [self performSelectorInBackground:@selector(reloadTableData) withObject:nil];
}

-(void)reloadTableData
{
    [pull setState:PullToRefreshViewStateLoading];
    [self performSelectorOnMainThread:@selector(viewDidLoad) withObject:nil waitUntilDone:NO];
    
    
}
//
//-(void)foregroundRefresh:(NSNotification *)notification
//{
//  
//        self.dataTbl2.contentOffset = CGPointMake(0, -65);
//        [pull setState:PullToRefreshViewStateLoading];
//        [self performSelectorInBackground:@selector(reloadTableData) withObject:nil];
//  
//}

#pragma mark -
#pragma mark - NSNnotification method

-(void)refreshData2:(NSNotification *)object
{
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:kReceiveData2
                                                 object:nil];
    
    self.pageArrSecondView = (NSArray *)[object object];
    

    SecondURLConn2 *urlContrl = [[SecondURLConn2 alloc]init];
    [urlContrl startConnection:[NSURL URLWithString:SecondURLName]];
    
   // NSLog(@"PAGE ARRAY IS: %d", [pageArrSecondView count]);
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(receiveDataSecond:)
                                                name:kReceiveDataSecond2
                                              object:nil];
}

-(void)receiveDataSecond:(NSNotification*)body
{
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:kReceiveDataSecond2
                                                 object:nil];
    
    self.pageArrSecondParse = (NSArray*)[body object];
    
    //NSLog(@"Data For Second Parse is:%@", self.pageArrSecondParse);
    
    [self performSelector:@selector(loadImagesForOnscreenRows) withObject:nil afterDelay:1.0];
    
    static dispatch_once_t oncePull;
    dispatch_once(&oncePull, ^{
        
        pull = [[PullToRefreshView alloc]initWithScrollView:(UIScrollView*)self.dataTbl2];
        [pull setDelegate:self];
        [self.dataTbl2 addSubview:pull];
        
        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(foregroundRefresh:)
//                                                     name:UIApplicationWillEnterForegroundNotification
//                                                   object:nil];
        
    });
    
    [pull finishedLoading];
    [pull setState:PullToRefreshViewStateNormal];
    [self.dataTbl2 reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[HUD removeFromSuperview];
	HUD = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

#pragma mark - tableview datasouce

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([self.pageArrSecondView count] > 30)
    {
        return 30;
    }
    else
       return [self.pageArrSecondView count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 3)
    {
        return 50;
    }
    return 86;
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier  = @"SecondCustom";
    static NSString *PlaceHolderIdentifier = @"placeHolder";
    static NSString *AdsIdentifier = @"SecondAds";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PlaceHolderIdentifier];
    int nodeCount = [self.pageArrSecondView count];
    
    if (nodeCount == 0 && indexPath.row == 0)
    {
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:PlaceHolderIdentifier];
        }
        
        cell.textLabel.text = @"Loading";
        //cell.imageView.image = [UIImage imageNamed:@"appleLogo.png"];
        
        return cell;
    }
    
    else
    {
        if (indexPath.row == 3)
        {
            MoleSecondAdsCell *cell = (MoleSecondAdsCell *)[tableView dequeueReusableCellWithIdentifier:AdsIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MoleFirstAdsCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            [cell addSubview:bannerView_];
            
            // Initiate a generic request to load it with an ad.
            [bannerView_ loadRequest:[GADRequest request]];
            return cell;
        }
        
        MoleSecondCustomCell *cell = (MoleSecondCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MoleSecondCustomCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
            
        }
        
        MoleProperties2 *moleCntrl = [self.pageArrSecondView objectAtIndex:indexPath.row];
        
        cell.titleLabel.textColor = [UIColor blackColor];
        cell.dateLabel.textColor  = [UIColor grayColor];
        cell.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17.0];
        cell.titleLabel.text = moleCntrl.title;
        cell.dateLabel.text  = moleCntrl.date;
        
        
        
        if (moleCntrl.moleIcon)
        {
            cell.thumbnailImageView.image = moleCntrl.moleIcon;
            return cell;
        }
        else
        {
            cell.thumbnailImageView.image = [UIImage imageNamed:@"moleTableIcon.png"];
        }
        
        return cell;
        
    }
    
    return cell;

}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //NSLog(@"Did Ened Decelerate");
    
    [self loadImagesForOnscreenRows];
}

#pragma mark - tableview delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    viewController_ = [[MoleDetail2VC  alloc]initWithNibName:@"MoleDetail2VC" bundle:nil];
    
    MoleProperties2 *controllerMole = [self.pageArrSecondView objectAtIndex:indexPath.row];
    
    MolePropertiesSecond2 *moleCntroller = [self.pageArrSecondParse objectAtIndex:indexPath.row];
    
    _LinkForRead = [[NSMutableArray alloc]init];
    
    NSData *ipData = [[NSData alloc]initWithContentsOfURL:
                      [NSURL URLWithString:controllerMole.image2]];
    
    UIImage *image = [UIImage imageWithData:ipData];

    [viewController_ setStoryTitle:moleCntroller.title2];
    [viewController_ setDescription:moleCntroller.body];
    [viewController_ setPageImage2:image];
    [viewController_ setLink:controllerMole.link];
    
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:moleCntroller.body options:0 range:NSMakeRange(0, [moleCntroller.body length])];
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            NSURL *url = [match URL];
            
            if (_LinkForRead != nil)
            {
                [viewController_ setLinkURL:url];
                [_LinkForRead removeAllObjects];
                [_LinkForRead addObject:url];
                [viewController_ setLinkForReadMore:_LinkForRead];
            } else {
                
                [viewController_ setLinkURL:url];
                [_LinkForRead addObject:url];
                [viewController_ setLinkForReadMore:_LinkForRead];
                //NSLog(@"found URL: %@", url);
                
            }
            
        }
    }
    
    
    
    //NSLog(@"LINK FOR READ %@", _LinkForRead);
    
    viewController_.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController_ animated:YES];
    
    [dataTbl2 deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Load image method

- (void)loadImagesForOnscreenRows
{
    
    if ([self.pageArrSecondView count] > 0)
    {
        NSArray *visiblePaths         = [self.dataTbl2 indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            
            MoleProperties2 *moleItem = [self.pageArrSecondView objectAtIndex:indexPath.row];
            
            if (!queue)
            {
                queue = dispatch_queue_create("image_queue", NULL);
            }
            
            dispatch_async(queue, ^
                           {
                               
                               NSString *imageString = moleItem.image2;
                               
                               //NSLog(@"AAAAAAA: %@",moleItem.image3);
                               
                               NSData   *data        = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageString]];
                               
                               UIImage  *anImage     = [UIImage imageWithData:data];
                               
                               MoleSecondCustomCell *cell = (MoleSecondCustomCell*)[self.dataTbl2 cellForRowAtIndexPath:indexPath];
                               
                               //dispatch_async on the main queue to update the UI
                               dispatch_async(dispatch_get_main_queue(), ^
                                              {
                                                  
                                                  if (anImage != nil)
                                                  {
                                                      [cache setObject:anImage forKey:moleItem.moleIcon];
                                                  
                                                  
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                                  
                                                      if (indexPath.row != 3)
                                                      {
                                                          cell.thumbnailImageView.image = anImage;
                                                      }
                                                      
                                                  }
                                                  
                                                 // NSLog(@"Index Path Reload : %@" ,anImage);
                                                 // [self.dataTbl2 reloadRowsAtIndexPaths:[NSArray arrayWithArray:visiblePaths] withRowAnimation:UITableViewRowAnimationNone];
                                                  //[table2 reloadRowsAtIndexPaths:[NSArray arrayWithArray:visiblePaths] withRowAnimation:UITableViewRowAnimationNone];
                                              });
                           });
        }
        
    }
}



@synthesize pageArrSecondView;
@synthesize dataTbl2;
@synthesize viewController = viewController_;
@synthesize pageArrSecondParse;
@synthesize cache;
@synthesize pull;

@end
