//
//  MoleThirdViewControllerViewController.h
//  Mole
//
//  Created by Wan Rahafiz on 9/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToRefreshView.h"
#import "MBProgressHUD.h"
#import "GADBannerView.h"


@class MoleDetail3ViewController;

@interface MoleThirdViewControllerViewController : UIViewController <UIWebViewDelegate,UITableViewDataSource,UITabBarDelegate, PullToRefreshViewDelegate, MBProgressHUDDelegate>

{
   
    //IBOutlet MoleThirdWebViewViewController *viewController;
    
    IBOutlet MoleDetail3ViewController *viewController;
    
   
    MBProgressHUD *HUD;
    
    
    GADBannerView *bannerView_;
    

    dispatch_queue_t queue;
}

#pragma mark - property declaration

@property (nonatomic, strong) IBOutlet UITableView           *dataTbl3;

@property (nonatomic, strong)          NSArray               *pageArrThirdView;

@property (nonatomic, strong)          NSCache               *cache;

@property (nonatomic, strong)          NSArray               *pageArrSecondParse;


@property (nonatomic, strong)          MoleDetail3ViewController *viewController;

@property (nonatomic, strong)          NSMutableArray *LinkForRead;

#pragma mark - method declaration


-(void)reloadTableData;

//-(void)foregroundRefresh:(NSNotification*)notification;

//- (void)myTask;

@end
