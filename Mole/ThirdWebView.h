//
//  ThirdWebView.h
//  Mole
//
//  Created by Lorenzo Jose on 3/31/13.
//
//

#import <UIKit/UIKit.h>

@interface ThirdWebView : UIViewController <UIWebViewDelegate>
{
    
}

@property (nonatomic, strong) IBOutlet UIWebView *thirdWebView;
@property (nonatomic, strong)          NSURL           *linkToPage;

@end
