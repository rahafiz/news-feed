//
//  MoleAppDelegate.h
//  Mole
//
//  Created by Wan Rahafiz on 9/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MoleAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>
{
     
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;

@property (strong, nonatomic) UINavigationController *navigate;



@end
