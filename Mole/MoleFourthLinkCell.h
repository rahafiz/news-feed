//
//  MoleFourthLinkCell.h
//  Mole
//
//  Created by Lorenzo Jose on 3/31/13.
//
//

#import <UIKit/UIKit.h>

@interface MoleFourthLinkCell : UITableViewCell
{
    
}

@property (nonatomic, strong) IBOutlet UILabel *readMoreLbl;


@property (nonatomic, strong)          NSURL *LinkURL;

@end
