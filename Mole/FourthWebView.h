//
//  FourthWebView.h
//  Mole
//
//  Created by Lorenzo Jose on 3/31/13.
//
//

#import <UIKit/UIKit.h>

@interface FourthWebView : UIViewController <UIWebViewDelegate>
{
    
}

@property (nonatomic, strong) IBOutlet UIWebView *fourthWebView;
@property (nonatomic, strong)          NSURL           *linkToPage;

@end
