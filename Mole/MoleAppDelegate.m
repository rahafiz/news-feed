//
//  MoleAppDelegate.m
//  Mole
//
//  Created by Wan Rahafiz on 9/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MoleAppDelegate.h"

#import "MoleFirstViewController.h"

#import "MoleSecondViewController.h"

#import "MoleThirdViewControllerViewController.h"

#import "MoleFourthViewController.h"

#import "MoleFifthViewController.h"
#import <FacebookSDK/FacebookSDK.h>



@implementation MoleAppDelegate

@synthesize window = _window;
@synthesize tabBarController = _tabBarController;
@synthesize navigate = navigate_;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    UIViewController *viewController1, *viewController2, *viewController3,*viewController4,*viewController5;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
    {
        viewController1 = [[MoleFirstViewController alloc] initWithNibName:@"MoleFirstViewController_iPhone" bundle:nil];
        viewController2 = [[MoleSecondViewController alloc] initWithNibName:@"MoleSecondViewController_iPhone" bundle:nil];
        viewController3 = [[MoleThirdViewControllerViewController alloc]initWithNibName:@"MoleThirdViewController_iPhone" bundle:nil];
        viewController4 = [[MoleFourthViewController alloc]initWithNibName:@"MoleFourthViewController_iPhone" bundle:nil];
        viewController5 = [[MoleFifthViewController alloc]initWithNibName:@"MoleFifthViewController_iPhone" bundle:nil];
    } 
    else 
    {
        viewController1 = [[MoleFirstViewController alloc] initWithNibName:@"MoleFirstViewController_iPad" bundle:nil];
        viewController2 = [[MoleSecondViewController alloc] initWithNibName:@"MoleSecondViewController_iPad" bundle:nil];
        viewController3 = [[MoleThirdViewControllerViewController alloc]initWithNibName:@"MoleThirdViewController_iPad" bundle:nil];
        viewController4 = [[MoleFourthViewController alloc]initWithNibName:@"MoleFourthViewController_iPad" bundle:nil];
        viewController5 = [[MoleFifthViewController alloc]initWithNibName:@"MoleFifthViewController_iPad" bundle:nil];
    }
   
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.tabBar.tintColor = [UIColor viewFlipsideBackgroundColor];
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:viewController1, viewController2,viewController3,viewController4,viewController5, nil];
  
    
    /* Initialize navigation controllers */
    UINavigationController * navigationController1 = [[UINavigationController alloc] initWithRootViewController:viewController1];
    UINavigationController * navigationController2 = [[UINavigationController alloc] initWithRootViewController:viewController2];
    UINavigationController * navigationController3 = [[UINavigationController alloc] initWithRootViewController:viewController3];
    UINavigationController * navigationController4 = [[UINavigationController alloc] initWithRootViewController:viewController4];
    UINavigationController * navigationController5 = [[UINavigationController alloc] initWithRootViewController:viewController5];
    
    navigationController1.navigationBar.tintColor = [UIColor orangeColor];
    navigationController2.navigationBar.tintColor = [UIColor orangeColor];
    navigationController3.navigationBar.tintColor = [UIColor orangeColor];
    navigationController4.navigationBar.tintColor = [UIColor orangeColor];
    navigationController5.navigationBar.tintColor = [UIColor orangeColor];
    //[navigationController1 setNavigationBarHidden:YES];
    //[navigationController2 setNavigationBarHidden:YES];
    //[navigationController3 setNavigationBarHidden:YES];
    
//    navigationController1.navigationBar.tintColor = [UIColor blackColor];
//    navigationController2.navigationBar.tintColor = [UIColor blackColor];
//    navigationController3.navigationBar.tintColor = [UIColor lightTextColor];
//    navigationController4.navigationBar.tintColor = [UIColor lightTextColor];
//    navigationController5.navigationBar.tintColor = [UIColor lightTextColor];
    
    
    //[navigationController4 setNavigationBarHidden:YES];
    //[navigationController5 setNavigationBarHidden:YES];
    
    // Create image for navigation background - portrait
    UIImage *MoleNavigationPortraitBackground = [[UIImage imageNamed:@"moleNavLogo.png"]
    resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    // Create image for navigation background - landscape
    UIImage *MoleNavigationLandscapeBackground = [[UIImage imageNamed:@"logoMole.png"]
    resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    // Set the background image all UINavigationBars
    [[UINavigationBar appearance] setBackgroundImage:MoleNavigationPortraitBackground
    forBarMetrics:UIBarMetricsDefault];
     [[UINavigationBar appearance] setBackgroundImage:MoleNavigationLandscapeBackground
    forBarMetrics:UIBarMetricsLandscapePhone];

    
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:navigationController1,navigationController2,navigationController3,navigationController4,navigationController5, nil];
    
   
    [viewController1 release];
    [viewController2 release];
    [viewController3 release];
    [viewController4 release];
    [viewController5 release];
    [navigationController1 release];
    [navigationController2 release];
    [navigationController3 release];
    [navigationController4 release];
    [navigationController5 release];
    

    //[_tabBarController.viewControllers release];
    self.window.rootViewController = self.tabBarController;
    //[self.tabBarController.tabBar.tintColor release];
    //[self.tabBarController release];
    //[self.window.rootViewController release];
    

    [self.window makeKeyAndVisible];
    return YES;
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        UIImageView *defaultImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default@2x.png"]];
        [self.window addSubview:defaultImageView];
        sleep(2);
        [defaultImageView removeFromSuperview];
    
    [defaultImageView release];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

#pragma mark -
#pragma mark - Facebook configuration

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{

    
    return [FBSession.activeSession handleOpenURL:url];
    
    
}

- (void)dealloc
{
    if(![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(dealloc)
                               withObject:nil
                            waitUntilDone:[NSThread isMainThread]];
        return;
    }
    [super dealloc];
    
}





@end
