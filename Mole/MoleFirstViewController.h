//
//  MoleFirstViewController.h
//  Mole
//
//  Created by Wan Rahafiz on 9/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToRefreshView.h"
#import "MBProgressHUD.h"
#import "GADBannerView.h"



@class MoleDetail1VC;

@interface MoleFirstViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MBProgressHUDDelegate, PullToRefreshViewDelegate>
{
    IBOutlet MoleDetail1VC *viewController;
    
    dispatch_queue_t queue;
    
    
    MBProgressHUD *HUD;
    
    NSMutableArray *dummyArr;
    
    NSUserDefaults *defaults;
    
    GADBannerView *bannerView_;
    
   

}

#pragma mark - property declaration

@property (nonatomic, strong) IBOutlet UITableView   *dataTbl1;

@property (nonatomic, strong)          NSArray       *pageArrFirstView;

@property (nonatomic, strong)          NSArray       *pageArrSecondParse;

@property (nonatomic, strong)          NSCache       *cache;

@property (nonatomic, strong)          MoleDetail1VC *viewController;

@property (nonatomic, strong)          NSMutableArray *dummyArr;

@property (nonatomic, strong)          NSUserDefaults *defaults;

@property (nonatomic, strong)          NSMutableArray *LinkForRead;





#pragma mark - method declaration

//-(void)requestURL:(NSURL*)url;
//
//-(void)reloadTableData;
//
//-(void)foregroundRefresh:(NSNotification*)notification;

//-(void)showWIthLabelAnnularDeterminate:(id)sender;

-(void)reloadTableData;

//-(void)foregroundRefresh:(NSNotification*)notification;

//- (void)myTask;


@end
