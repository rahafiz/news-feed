//
//  MoleSecondViewController.h
//  Mole
//
//  Created by Wan Rahafiz on 9/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToRefreshView.h"
#import "MBProgressHUD.h"
#import "GADBannerView.h"

@class MoleDetail2VC;

@interface MoleSecondViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MBProgressHUDDelegate, PullToRefreshViewDelegate>
{
    IBOutlet MoleDetail2VC *viewController;
    
    dispatch_queue_t queue;
    
    MBProgressHUD *HUD;
    
    GADBannerView *bannerView_;
    
    long long expectedLength;
    long long currentLength;
}

#pragma mark - property declaration

@property (nonatomic, strong) IBOutlet UITableView   *dataTbl2;

@property (nonatomic, strong)          NSArray       *pageArrSecondView;

@property (nonatomic, strong)          NSArray       *pageArrSecondParse;

@property (nonatomic, strong)          NSCache       *cache;

@property (nonatomic, strong)          MoleDetail2VC *viewController;

@property (nonatomic, strong)          NSMutableArray *LinkForRead;

#pragma mark - method declaration

//-(void)requestURL:(NSURL*)url;
//
//-(void)reloadTableData;
//
//-(void)foregroundRefresh:(NSNotification*)notification;

-(void)reloadTableData;

//-(void)foregroundRefresh:(NSNotification*)notification;

//- (void)myTask;

@end
