//
//  MoleFirstImageCell.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <UIKit/UIKit.h>

@interface MoleFirstImageCell : UITableViewCell
{
    
}

@property (nonatomic, strong) IBOutlet UIImageView *firstImage;

@end
