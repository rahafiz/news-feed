//
//  MoleThirdCustomDetailCell.h
//  Mole
//
//  Created by Wan Rahafiz on 10/15/12.
//
//

#import <UIKit/UIKit.h>

@interface MoleThirdCustomDetailCell : UITableViewCell
{
    
}

#pragma mark - property declaration

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@end
