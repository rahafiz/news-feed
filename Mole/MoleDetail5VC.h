//
//  MoleDetail5VC.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "GADBannerView.h"
#import "FifthWebView.h"


@interface MoleDetail5VC : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>
{
    NSArray *arrayDetailContent;
    
    UIImageView *pageImage;
    UITextView  *contentText;
    UILabel     *titleLbl;
    
    NSMutableArray *mArraySection;
    NSMutableArray *mArrayDetails;
    
    NSString *storyTitle;
    NSString *description;
    NSString *link;
    
    GADBannerView *bannerView_;
    
    // UIToolbar *toolBar;
}

#pragma mark - properties declaration

@property (nonatomic, strong) IBOutlet UILabel       *titleLbl;
//@property (nonatomic, strong) IBOutlet UIImageView   *pageImage;
@property (nonatomic, strong) IBOutlet UITextView    *contentText;
@property (nonatomic, strong) IBOutlet UITableView   *detailTbl5;

@property (nonatomic, strong)          NSArray       *arrayDetailContent;

@property (nonatomic, strong)          NSMutableArray *mArrayDetails;

@property (nonatomic, strong)          NSMutableArray *linkForReadMore;

@property (nonatomic, strong)          NSString      *storyTitle;

@property (nonatomic, strong)          NSString      *description;

@property (nonatomic, strong)          NSString      *link;

@property (nonatomic, strong)          UIImage   *pageImage5;

@property (nonatomic, strong)          NSURL *LinkURL;

@property (nonatomic, strong)          UILabel *labelForStr;

@property (nonatomic, strong)          FifthWebView *fifthWebVC;

//@property (nonatomic, strong)          UIToolbar *toolBar;


#pragma mark - method declaration

-(IBAction)showActionSheet:(id)sender;
-(void)facebookButtonClicked:(id)sender;
-(void)twitterButtonClicked:(id)sender;
-(void)openMail:(id)sender;
-(void)emailButtonClicked:(id)sender;

@end
