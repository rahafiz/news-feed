//
//  FifthWebView.h
//  Mole
//
//  Created by Lorenzo Jose on 3/31/13.
//
//

#import <UIKit/UIKit.h>

@interface FifthWebView : UIViewController <UIWebViewDelegate>
{
    
}

@property (nonatomic, strong) IBOutlet UIWebView *fifthWebView;
@property (nonatomic, strong)          NSURL           *linkToPage;

@end
