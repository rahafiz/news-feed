//
//  MoleThirdImage.h
//  Mole
//
//  Created by Wan Rahafiz on 10/16/12.
//
//

#import <UIKit/UIKit.h>

@interface MoleThirdImage : UITableViewCell
{
    
}

@property (nonatomic, strong) IBOutlet UIImageView *thirdImage;

@end
