//
//  FirstWebView.h
//  Mole
//
//  Created by Lorenzo Jose on 3/30/13.
//
//

#import <UIKit/UIKit.h>

@interface FirstWebView : UIViewController <UIWebViewDelegate>
{
 
}

@property (nonatomic, strong) IBOutlet UIWebView *firstWebView;
@property (nonatomic, strong)          NSURL           *linkToPage;



@end
