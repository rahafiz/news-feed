//
//  MoleThirdDetailsCustomCell.m
//  Mole
//
//  Created by Wan Rahafiz on 10/15/12.
//
//

#import "MoleThirdDetailsCustomCell.h"

@implementation MoleThirdDetailsCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
       
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@synthesize detailLabel = _detailLabel;

@end
