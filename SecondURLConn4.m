//
//  SecondURLConn4.m
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import "SecondURLConn4.h"
#import "SecondParseOperation4.h"
#import "Constant.h"

@implementation SecondURLConn4

-(void)startConnection:(NSURL *)url
{
    self.pageArr   = [NSMutableArray array];
    self.tableArr  = [NSMutableArray array];
    
    //NSLog(@"Connection start For Second Time");
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    self.pageConn         = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    if (self.pageConn == nil)
    {
        [self performSelector:@selector(handleErrorConn) withObject:pageConn];
    }
    
    
}

-(void)handleErrorConn
{
    NSError *error;
    NSString     *errorMessage = [error localizedDescription];
    UIAlertView  *alertView   = [[UIAlertView alloc]initWithTitle:@"Can't establish connection"
                                                          message:errorMessage
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
    
    [alertView show];
}

-(void)didFinishParsing: (NSArray *)pageList
{
    [self performSelectorOnMainThread:@selector(handleLoads:) withObject:pageList waitUntilDone:NO];
    
    self.queue = nil;
}

-(void)handleLoads:(NSArray *)dataList
{
    [self.pageArr removeAllObjects];
    [self.pageArr addObjectsFromArray:dataList];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kReceiveDataSecond4
                                                        object:pageArr
                                                      userInfo:nil];
    
   // NSLog(@"SELF PAGE ARR IS:%@", self.pageArr);
    
}

-(void)parseErrorOccurred:(NSError *)error
{
    
    [self performSelectorOnMainThread:@selector(handleError:) withObject:error waitUntilDone:NO];
}


- (void)handleError:(NSError *)error
{
    NSString *errorMessage = [error localizedDescription];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"http error"
														message:errorMessage
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
    [alertView show];
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.pageData = [NSMutableData data];    // start off with new data
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    [pageData appendData:data];  // append incoming data
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ([error code] == kCFURLErrorNotConnectedToInternet)
	{
        // if we can identify the error, we can present a more precise message to the user.
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@ \r %@ \r %@",@"Unable to download new content because your iphone does not have connectivity to the Internet", @"Please retry again when you establish connectivity",@"Thank You"]
															 forKey:NSLocalizedDescriptionKey];
        
        NSError *noConnectionError = [NSError errorWithDomain:NSCocoaErrorDomain
														 code:kCFURLErrorNotConnectedToInternet
													 userInfo:userInfo];
        [self handleError:noConnectionError];
    }
	else
	{
        // otherwise handle the error generically
        [self handleError:error];
    }
    
    self.pageConn = nil;   // release our connection
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //self.megazineConnection = nil;   // release our connection
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    // create the queue to run our ParseOperation
    self.queue = [[NSOperationQueue alloc] init];
    
    // create an ParseOperation (NSOperation subclass) to parse the RSS feed data so that the UI is not blocked
    // "ownership of appListData has been transferred to the parse operation and should no longer be
    // referenced in this thread.
    //
    SecondParseOperation4 *parser = [[SecondParseOperation4 alloc] initWithData:pageData delegate:self];
    
    
    
    [queue addOperation:parser]; // this will start the "ParseOperation"
    
    
    
    
    // ownership of appListData has been transferred to the parse operation
    // and should no longer be referenced in this thread
    self.pageData = nil;
}

@synthesize pageConn, pageArr, pageData, tableArr;
@synthesize queue;
@synthesize moleController;

@end
