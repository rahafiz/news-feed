//
//  SecondURLConn1.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <Foundation/Foundation.h>
#import "SecondParseOperation1.h"


@class MoleFirstViewController;

@interface SecondURLConn1 : NSObject <SecondParseOperation1Delegate>
{
    NSURLConnection         *pageConn;
    NSOperationQueue        *queue;
    NSMutableArray          *pageArr;
    NSMutableArray          *tableArr;
    
    NSMutableData           *pageData;
    
    NSHTTPURLResponse       *responseData;
    
    MoleFirstViewController *moleController;
    

    
}

#pragma mark - property declaration

@property (nonatomic, strong) NSURLConnection         *pageConn;
@property (nonatomic, strong) NSOperationQueue        *queue;
@property (nonatomic, strong) NSMutableArray          *pageArr;
@property (nonatomic, strong) NSMutableArray          *tableArr;

@property (nonatomic, strong) NSMutableData           *pageData;
//@property (nonatomic, strong) NSMutableData           *progressData;

@property (nonatomic, strong) MoleFirstViewController *moleController;




#pragma mark - method declaration

-(void)startConnection:(NSURL *)url;



@end
