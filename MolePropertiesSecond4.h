//
//  MolePropertiesSecond4.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <Foundation/Foundation.h>

@interface MolePropertiesSecond4 : NSObject
{
    ////////////////////////////////////Second Parse
    
    NSString *title4;
    NSString *created;
    NSString *body;
    
}

///////////////////////////////////////Second Parse

@property (nonatomic, strong) NSString  *title4;
@property (nonatomic, strong) NSString  *created;
@property (nonatomic, strong) NSString  *body;

@end
