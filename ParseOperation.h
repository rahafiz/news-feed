//
//  ParseOperation.h
//  Mole
//
//  Created by Wan Rahafiz on 9/25/12.
//
//

#import <Foundation/Foundation.h>

@class MoleProperties;
@class URLConnFirstView;

@protocol ParseOperationDelegate;


@interface ParseOperation : NSOperation <NSXMLParserDelegate>
{
@private
    id <ParseOperationDelegate> delegate;
    
    NSData              *dataToParse;
    
    NSArray             *elementsToParse;
    NSMutableArray      *workingArray;
    NSMutableString     *results;
    
    
    BOOL                storingCharacterData;
    MoleProperties      *workingEntry;
    //URLConnFirstView    *appDelegate;
}


- (id)initWithData:(NSData *)data delegate:(id <ParseOperationDelegate>)theDelegate;

@end

@protocol ParseOperationDelegate
- (void)didFinishParsing:(NSArray *)appList;
- (void)parseErrorOccurred:(NSError *)error;

@end


