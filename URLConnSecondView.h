//
//  URLConnSecondView.h
//  Mole
//
//  Created by Wan Rahafiz on 9/25/12.
//
//

#import <Foundation/Foundation.h>
#import "ParseOperation2.h"

@class MoleSecondViewController;



@interface URLConnSecondView : NSObject <ParseOperation2Delegate>
{
    NSURLConnection         *pageConn;
    NSOperationQueue        *queue;
    NSMutableArray          *pageArr;
    NSMutableArray          *tableArr;
    
    NSMutableData           *pageData;
    
    MoleSecondViewController *moleController;
    
    float totalSize;
    long long receiveData;
    
    float progressLoading;
    
}

#pragma mark - property declaration

@property (nonatomic, strong) NSURLConnection         *pageConn;
@property (nonatomic, strong) NSOperationQueue        *queue;
@property (nonatomic, strong) NSMutableArray          *pageArr;
@property (nonatomic, strong) NSMutableArray          *tableArr;

@property (nonatomic, strong) NSMutableData           *pageData;

@property (nonatomic, strong) MoleSecondViewController *moleController;

#pragma mark - method declaration

-(void)startConn:(NSURL *)url;
@end
