//
//  SecondURLConn5.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <Foundation/Foundation.h>
#import "SecondParseOperation5.h"

@class MoleFifthViewController;

@interface SecondURLConn5 : NSObject <SecondParseOperation5Delegate>
{
    NSURLConnection         *pageConn;
    NSOperationQueue        *queue;
    NSMutableArray          *pageArr;
    NSMutableArray          *tableArr;
    
    NSMutableData           *pageData;
    
    MoleFifthViewController *moleController;
    
}

#pragma mark - property declaration

@property (nonatomic, strong) NSURLConnection         *pageConn;
@property (nonatomic, strong) NSOperationQueue        *queue;
@property (nonatomic, strong) NSMutableArray          *pageArr;
@property (nonatomic, strong) NSMutableArray          *tableArr;

@property (nonatomic, strong) NSMutableData           *pageData;

@property (nonatomic, strong) MoleFifthViewController *moleController;

#pragma mark - method declaration

-(void)startConnection:(NSURL *)url;



@end
