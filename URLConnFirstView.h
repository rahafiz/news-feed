//
//  URLConnFirstView.h
//  Mole
//
//  Created by Wan Rahafiz on 9/25/12.
//
//

#import <Foundation/Foundation.h>
#import "ParseOperation.h"
#import "MBProgressHUD.h"





@class MoleFirstViewController;


@interface URLConnFirstView : NSObject <ParseOperationDelegate>
{
    NSURLConnection         *pageConn;
    NSOperationQueue        *queue;
    NSMutableArray          *pageArr;
    NSMutableArray          *tableArr;
    
    NSMutableData           *pageData;
    
    MoleFirstViewController *moleController;
    
   
   
    
    
}

#pragma mark - property declaration

@property (nonatomic, strong) NSURLConnection         *pageConn;
@property (nonatomic, strong) NSOperationQueue        *queue;
@property (nonatomic, strong) NSMutableArray          *pageArr;
@property (nonatomic, strong) NSMutableArray          *tableArr;

@property (nonatomic, strong) NSMutableData           *pageData;

@property (nonatomic, strong) MoleFirstViewController *moleController;






#pragma mark - method declaration

-(void)startConn:(NSURL *)url;

@end
