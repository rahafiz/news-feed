//
//  SecondParseOperation2.m
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import "SecondParseOperation2.h"
#import "MolePropertiesSecond2.h"

//set the string for our data in xml file
static NSString *kNodeStr           = @"node";
static NSString *kTitleStr          = @"title";
static NSString *kCreatedStr        = @"created";
static NSString *kBodyStr           = @"body";


@interface SecondParseOperation2()
{
    
}

@property (nonatomic, readwrite) id <SecondParseOperation2Delegate> delegate;

@property (nonatomic, retain) NSData                 *dataToParse;
@property (nonatomic, retain) NSMutableArray         *workingArray;
@property (nonatomic, retain) NSArray                *elementsToParse;
@property (nonatomic, retain) NSMutableString        *workingPropertyString;


@property (nonatomic, assign) BOOL                    storingCharacterData;
@property (nonatomic, retain) MolePropertiesSecond2   *workingEntry;

@end

@implementation SecondParseOperation2

- (id)initWithData:(NSData *)data delegate:(id <SecondParseOperation2Delegate>)theDelegate
{
    //init our data when we start the parsing
    self = [super init];
    if (self != nil)
    {
        self.dataToParse = data;
        self.delegate = theDelegate;
        self.elementsToParse = [NSArray arrayWithObjects:kNodeStr, kTitleStr,
                                kCreatedStr, kBodyStr,nil];
        
        
    }
    return self;
}

- (void)main
{
    
	//array and string for hold data when parsing started
	self.workingArray = [[NSMutableArray alloc]init];
    self.workingPropertyString = [[NSMutableString alloc]init];;
    
    // It's also possible to have NSXMLParser download the data, by passing it a URL, but this is not
	// desirable because it gives less control over the network, particularly in responding to
	// connection errors.
    //
    
    //start parsing
    NSXMLParser *parser = [[NSXMLParser alloc]initWithData:dataToParse];
    [parser setDelegate:self];
    
    //check if the parsing successfully or not
    BOOL success = [parser parse];
    
    if (success) {
       // NSLog(@"NO ERROR3 FOR SECOND");
    }
    else {
        //NSLog(@"ERROR OCCUR FOR SECOND");
    }
    
    
	if (![self isCancelled])
    {
        // notify our AppDelegate that the parsing is complete
        [self.delegate didFinishParsing:self.workingArray];
        
    }
    
    self.workingArray = nil;
    self.workingPropertyString = nil;
    self.dataToParse = nil;
    
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    //NSLog(@"DID START DOCUMENT!");
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    
    
    
    if ([elementName isEqualToString:@"xml"]) {
        
        if (!self.workingArray) {
            self.workingArray = [[NSMutableArray alloc]init];
            
        }
        return;
        
    }
    if ([elementName isEqualToString:kNodeStr]){
        
        workingEntry = [[MolePropertiesSecond2 alloc]init];
        return;
        
    }
    if ([elementName isEqualToString:kTitleStr]){
        
        results = [[NSMutableString alloc]init];
        
        
    }
    if ([elementName isEqualToString:kCreatedStr]) {
        
        results = [[NSMutableString alloc]init];
    }
    
    if ([elementName isEqualToString:kBodyStr]) {
        results = [[NSMutableString alloc]init];
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
{
    
    
    
    if ([elementName isEqualToString:@"xml"]) {
        return;
    }
    if ([elementName isEqualToString:kNodeStr])
    {
        // NSLog(@"END ELEMENT: %@", kItemStr);
        [workingArray addObject:workingEntry];
        
        return;
        
        
    }
    if ([elementName isEqualToString:kTitleStr])
    {
        self.workingEntry.title2 = results;
        results = nil;
        
        
        // NSLog(@"news stand id : %@",workingEntry.title);
    }
    else if ([elementName isEqualToString:kCreatedStr])
    {
        
        self.workingEntry.created = results;
        
        
        
        
        results = nil;
        
        
    }
    
    else if ([elementName isEqualToString:kBodyStr])
    {
        
        // self.workingEntry.body = results;
        
        NSString *newStr       = [results stringByReplacingOccurrencesOfString:@"&nbsp;"
                                                                    withString:@" "];
        NSString *lastStr      = [newStr stringByReplacingOccurrencesOfString:@"&nbsp;"
                                                                   withString:@" "];
        NSString *finalStr     = [lastStr stringByReplacingOccurrencesOfString:@"Read more HERE."
                                                                    withString:@""];
        NSString *finalStr2    = [finalStr stringByReplacingOccurrencesOfString:@"Read more of Rocky’s posting HERE" withString:@""];
        NSString *finalStr3    = [finalStr2 stringByReplacingOccurrencesOfString:@"Read more at CHE DET" withString:@""];
        NSString *finalStr4    = [finalStr3 stringByReplacingOccurrencesOfString:@"READ MORE HERE" withString:@""];
        NSString *finalStr5    = [finalStr4 stringByReplacingOccurrencesOfString:@"Read more at ROCKY'S BRU" withString:@""];
        NSString *finalStr6    = [finalStr5 stringByReplacingOccurrencesOfString:@"read more HERE" withString:@""];
        
       // NSLog(@"FINAL STR IS %@", finalStr6);
        self.workingEntry.body = finalStr6;
        
        //NSLog(@"SECOND THING IS:%@", lastStr);
        
        results = nil;
        newStr  = nil;
        //stripped  = nil;
        //trimmedString = nil;
    }
    
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    
    
    if (!results) {
        results = [[NSMutableString alloc]initWithString:string];
    }
    else {
        [results appendString:string];
    }
    
    
    //NSLog(@"Results is: %@", results);
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    [delegate parseErrorOccurred:parseError];
}



@synthesize  dataToParse, workingArray, workingEntry, workingPropertyString, elementsToParse, storingCharacterData,delegate;



@end
