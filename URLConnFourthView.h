//
//  URLConnFourthView.h
//  Mole
//
//  Created by Wan Rahafiz on 9/26/12.
//
//

#import <Foundation/Foundation.h>
#import "ParseOperation4.h"

@class MoleFourthViewController;

@interface URLConnFourthView : NSObject <ParseOperation4Delegate>

{
    NSURLConnection         *pageConn;
    NSOperationQueue        *queue;
    NSMutableArray          *pageArr;
    NSMutableArray          *tableArr;
    
    NSMutableData           *pageData;
    
    MoleFourthViewController *moleController;
    
}

#pragma mark - property declaration

@property (nonatomic, strong) NSURLConnection          *pageConn;
@property (nonatomic, strong) NSOperationQueue         *queue;
@property (nonatomic, strong) NSMutableArray           *pageArr;
@property (nonatomic, strong) NSMutableArray           *tableArr;

@property (nonatomic, strong) NSMutableData            *pageData;

@property (nonatomic, strong) MoleFourthViewController *moleController;

#pragma mark - method declaration

-(void)startConn:(NSURL *)url;

@end
