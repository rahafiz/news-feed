//
//  MolePropertiesSecond2.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <Foundation/Foundation.h>

@interface MolePropertiesSecond2 : NSObject
{
    ////////////////////////////////////Second Parse
    
    NSString *title2;
    NSString *created;
    NSString *body;
    
}

///////////////////////////////////////Second Parse

@property (nonatomic, strong) NSString  *title2;
@property (nonatomic, strong) NSString  *created;
@property (nonatomic, strong) NSString  *body;

@end
