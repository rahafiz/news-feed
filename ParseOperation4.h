//
//  ParseOperation4.h
//  Mole
//
//  Created by Wan Rahafiz on 9/26/12.
//
//

#import <Foundation/Foundation.h>

@class MoleProperties4;
@class URLConnFourthView;

@protocol ParseOperation4Delegate;


@interface ParseOperation4 : NSOperation <NSXMLParserDelegate>
{
@private
    id <ParseOperation4Delegate> delegate;
    
    NSData              *dataToParse;
    
    NSArray             *elementsToParse;
    NSMutableArray      *workingArray;
    NSMutableString     *results;
    
    
    BOOL                storingCharacterData;
    MoleProperties4      *workingEntry;
   // URLConnFourthView    *appDelegate;
}


- (id)initWithData:(NSData *)data delegate:(id <ParseOperation4Delegate>)theDelegate;

@end

@protocol ParseOperation4Delegate
- (void)didFinishParsing:(NSArray *)appList;
- (void)parseErrorOccurred:(NSError *)error;

@end
