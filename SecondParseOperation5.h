//
//  SecondParseOperation5.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <Foundation/Foundation.h>

@class MolePropertiesSecond5;
@class SecondURLConn5;

@protocol SecondParseOperation5Delegate;

@interface SecondParseOperation5 : NSOperation <NSXMLParserDelegate>
{
@private
    id <SecondParseOperation5Delegate> delegate;
    
    NSData                *dataToParse;
    
    NSArray               *elementsToParse;
    NSMutableArray        *workingArray;
    NSMutableString       *results;
    
    
    BOOL                  storingCharacterData;
    MolePropertiesSecond5 *workingEntry;
    //SecondURLConn5        *appDelegate;
}


- (id)initWithData:(NSData *)data delegate:(id <SecondParseOperation5Delegate>)theDelegate;

@end

@protocol SecondParseOperation5Delegate
- (void)didFinishParsing:(NSArray *)appList;
- (void)parseErrorOccurred:(NSError *)error;

@end
