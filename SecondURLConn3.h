//
//  SecondURLConn3.h
//  Mole
//
//  Created by Wan Rahafiz on 10/9/12.
//
//

#import <Foundation/Foundation.h>
#import "SecondParseOperation3.h"

@class MoleThirdViewControllerViewController;

@interface SecondURLConn3 : NSObject <SecondParseOperation3Delegate>

{
    NSURLConnection         *pageConn;
    NSOperationQueue        *queue;
    NSMutableArray          *pageArr;
    NSMutableArray          *tableArr;
    
    NSMutableData           *pageData;
    
    MoleThirdViewControllerViewController *moleController;
    
}

#pragma mark - property declaration

@property (nonatomic, strong) NSURLConnection         *pageConn;
@property (nonatomic, strong) NSOperationQueue        *queue;
@property (nonatomic, strong) NSMutableArray          *pageArr;
@property (nonatomic, strong) NSMutableArray          *tableArr;

@property (nonatomic, strong) NSMutableData           *pageData;

@property (nonatomic, strong) MoleThirdViewControllerViewController *moleController;

#pragma mark - method declaration

-(void)startConnection:(NSURL *)url;

@end
