//
//  MolePropertiesSecond3.h
//  Mole
//
//  Created by Wan Rahafiz on 10/9/12.
//
//

#import <Foundation/Foundation.h>

@interface MolePropertiesSecond3 : NSObject
{
    ////////////////////////////////////Second Parse
    
    NSString *title3;
    NSString *created;
    NSString *body;
    
}

///////////////////////////////////////Second Parse

@property (nonatomic, strong) NSString  *title3;
@property (nonatomic, strong) NSString  *created;
@property (nonatomic, strong) NSString  *body;


@end
