//
//  ParseOperation5.h
//  Mole
//
//  Created by Wan Rahafiz on 9/26/12.
//
//

#import <Foundation/Foundation.h>

@class MoleProperties5;
@class URLConnFifthView;

@protocol ParseOperation5Delegate;


@interface ParseOperation5 : NSOperation <NSXMLParserDelegate>
{
@private
    id <ParseOperation5Delegate> delegate;
    
    NSData              *dataToParse;
    
    NSArray             *elementsToParse;
    NSMutableArray      *workingArray;
    NSMutableString     *results;
    
    
    BOOL                storingCharacterData;
    MoleProperties5      *workingEntry;
    //URLConnFifthView    *appDelegate;
}


- (id)initWithData:(NSData *)data delegate:(id <ParseOperation5Delegate>)theDelegate;

@end

@protocol ParseOperation5Delegate
- (void)didFinishParsing:(NSArray *)appList;
- (void)parseErrorOccurred:(NSError *)error;


@end
