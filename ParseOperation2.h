//
//  ParseOperation2.h
//  Mole
//
//  Created by Wan Rahafiz on 9/25/12.
//
//

#import <Foundation/Foundation.h>

@class URLConnSecondView;
@class MoleProperties2;

@protocol  ParseOperation2Delegate;

@interface ParseOperation2 : NSOperation <NSXMLParserDelegate>
{
@private
    id <ParseOperation2Delegate> delegate;
    
    NSData              *dataToParse;
    
    NSArray             *elementsToParse;
    NSMutableArray      *workingArray;
    NSMutableString     *results;
    
    
    BOOL                storingCharacterData;
    MoleProperties2     *workingEntry;
   // URLConnSecondView   *appDelegate;
}

- (id)initWithData:(NSData *)data delegate:(id <ParseOperation2Delegate>)theDelegate;

@end

@protocol ParseOperation2Delegate
- (void)didFinishParsing:(NSArray *)appList;
- (void)parseErrorOccurred:(NSError *)error;

@end


