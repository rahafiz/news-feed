//
//  SecondParseOperation2.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <Foundation/Foundation.h>

@class MolePropertiesSecond2;
@class SecondURLConn2;

@protocol SecondParseOperation2Delegate;

@interface SecondParseOperation2 : NSOperation <NSXMLParserDelegate>
{
@private
    id <SecondParseOperation2Delegate> delegate;
    
    NSData                *dataToParse;
    
    NSArray               *elementsToParse;
    NSMutableArray        *workingArray;
    NSMutableString       *results;
    
    
    BOOL                  storingCharacterData;
    MolePropertiesSecond2 *workingEntry;
    //SecondURLConn2        *appDelegate;
}


- (id)initWithData:(NSData *)data delegate:(id <SecondParseOperation2Delegate>)theDelegate;

@end

@protocol SecondParseOperation2Delegate
- (void)didFinishParsing:(NSArray *)appList;
- (void)parseErrorOccurred:(NSError *)error;

@end
