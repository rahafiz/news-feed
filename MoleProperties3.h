//
//  MoleProperties3.h
//  Mole
//
//  Created by Wan Rahafiz on 9/26/12.
//
//

#import <Foundation/Foundation.h>

@interface MoleProperties3 : NSObject

{
    NSString *title;
    NSString *link;
    NSString *description;
    NSString *language;
    NSString *item;
    NSString *date;
    
   
    
    NSString  *image3;
    
    NSString  *content;
    
    UIImage   *moleIcon;
    
    
   
}

#pragma mark - property declaration

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *language;
@property (nonatomic, strong) NSString *item;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *date;



@property (nonatomic, strong) NSString  *image3;

@property (nonatomic, strong) UIImage   *moleIcon;



@end
