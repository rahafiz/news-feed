//
//  SecondParseOperation4.h
//  Mole
//
//  Created by Wan Rahafiz on 10/30/12.
//
//

#import <Foundation/Foundation.h>

@class MolePropertiesSecond4;
@class SecondURLConn4;

@protocol SecondParseOperation4Delegate;

@interface SecondParseOperation4 : NSOperation <NSXMLParserDelegate>
{
@private
    id <SecondParseOperation4Delegate> delegate;
    
    NSData                *dataToParse;
    
    NSArray               *elementsToParse;
    NSMutableArray        *workingArray;
    NSMutableString       *results;
    
    
    BOOL                  storingCharacterData;
    MolePropertiesSecond4 *workingEntry;
    //SecondURLConn4        *appDelegate;
}


- (id)initWithData:(NSData *)data delegate:(id <SecondParseOperation4Delegate>)theDelegate;

@end

@protocol SecondParseOperation4Delegate
- (void)didFinishParsing:(NSArray *)appList;
- (void)parseErrorOccurred:(NSError *)error;

@end
