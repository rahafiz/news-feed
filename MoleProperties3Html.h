//
//  MoleProperties3Html.h
//  Mole
//
//  Created by Wan Rahafiz on 10/8/12.
//
//

#import <Foundation/Foundation.h>

@interface MoleProperties3Html : NSObject
{
    NSString *titleHtml;
    NSString *linkHtml;
    NSString *bodyHtml;
}

@property (nonatomic, strong) NSString *titleHtml;
@property (nonatomic, strong) NSString *linkHtml;
@property (nonatomic, strong) NSString *bodyHtml;

@end
