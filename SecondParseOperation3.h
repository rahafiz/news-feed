//
//  SecondParseOperation3.h
//  Mole
//
//  Created by Wan Rahafiz on 10/9/12.
//
//

#import <Foundation/Foundation.h>

@class MolePropertiesSecond3;
@class SecondURLConn3;

@protocol SecondParseOperation3Delegate;

@interface SecondParseOperation3 : NSOperation <NSXMLParserDelegate>
{
    @private
        id <SecondParseOperation3Delegate> delegate;
        
        NSData                *dataToParse;
        
        NSArray               *elementsToParse;
        NSMutableArray        *workingArray;
        NSMutableString       *results;
        
        
        BOOL                  storingCharacterData;
        MolePropertiesSecond3 *workingEntry;
        //SecondURLConn3        *appDelegate;
}
    
    
    - (id)initWithData:(NSData *)data delegate:(id <SecondParseOperation3Delegate>)theDelegate;
    
@end
    
    @protocol SecondParseOperation3Delegate
    - (void)didFinishParsing:(NSArray *)appList;
    - (void)parseErrorOccurred:(NSError *)error;
    

@end
